## II - SOLUCIÓN (2h)

La estrategía consiste en realizar un modelamiento de los tweets separandolos en dos dataframes:

- df_user: Correspondiente a usuarios tendría los siguientes features.

    - name
    - protected
    - screen_name
    - description

- df_tweet: correspondiente a tweets de los usuarios tendría los siguientes features.

    - name
    - created_at: fecha-hora de creación del tweet
    - text: texto del tweet
    - retweeted: booleano que indica si el tweet es un retweet
    - location: ubicación del tweet


**Género del usuario**

A partir de este dataframe se analizaría los datos para obtener el género del usuario.

La primer forma de predecir es con el nombre, en este caso, se compararía el nombre contra una base de datos de nombres clasificados por sexo. 

En caso de no contar con esta base de datos pre-construida, y entendiendo que no es una tarea compleja, lanzar un proyecto de etiquetado de datos para un 20% del dataset entre los miembros del equipo, permitiendo que un mismo usuario tenga la posibilidad de ser etiquetado por al menos dos personas. 

A partir de los resultados del proceso de etiquetado de datos. A partir de los resultados, en caso que sea necesario balancear las clases, incrementar las muestras a la clase que lo requiere. Con este dataset entrenar un modelo de clasificación con el cual se puedan predecir los demás nombre. Toda la clasificación se haría en 4 iteraciones posteriores al primer set de entrenamiento, de tal manera que con aprendizaje activo al finalizar el proceso se tenga una alta precisión para definir el sexo de una cuenta.

**Edad del usuario**

En relación a la edad, se usaría el modelo previamente entrenado en la prueba I para deducir la edad usando los textos de los tweets asociados con el usuario e incluiría un proceso de aprendizaje activo para garantizar que el modelo mantenga un accuracy adecuado y tenga en cuenta los nuevos tweets del dataset. Para cada instancia del df_tweets tendría un resultado de edad.

Posteriormente basado en la proporción de tweets clasificados con cierta edad se asociaría la clase de edad con la mayor frecuencia.

 **Ubicación del usuario**

 Similar a la edad del usuario, en este caso, basada en la frecuencia de localización del tweet se definiría la ubicación del usuario. En este caso se usa el df_tweets el cual contiene los tweets con su localización. Para cada usuario único en df_user se realiza una estadística descriptiva de la localización de los tweets asociados y se escoge la clase con la mayor frecuencia.
