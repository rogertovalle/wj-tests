#start
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import itertools
import re
import nltk
from nltk.corpus import stopwords
import itertools
from sklearn.feature_extraction.text import CountVectorizer
from nltk.tokenize import RegexpTokenizer
from nltk.stem import PorterStemmer
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from imblearn.over_sampling import RandomOverSampler


class analizeTweets():

    '''
    Description: This class process a given text of tweet.

    '''

    def __init__(self, df_tweets, analize="Both", other_stop_words=[]):

        self.df = df_tweets
        self.other_stop_words = other_stop_words
        self.analize = analize
    
    def normalize(self, text):
        
        '''
        Description: Normalize a given line avoiding accents.

        Attributes
        ----------
        message: str
                The line of message that has to be normalized.
        Returns
        ---------

        message: str
            Normaliced version of the message.

         '''
        
        replacements = (
            ("á", "a"),
            ("é", "e"),
            ("í", "i"),
            ("ó", "o"),
            ("ú", "u"),
            ("Á", "a"),
            ("É", "e"),
            ("Í", "i"),
            ("Ó", "o"),
            ("Ú", "u"),
        )
        for a, b in replacements:
            text = text.replace(a, b).replace(a.upper(), b.upper())
        
        return text

    
    
    
    def getStopWords(self):

        '''
        Description: Create a list of stopwords defined by nlkt library and defined by the user.

        Attributes:
        self.other_stop_words: List of stop words provided to the class at the beggining, defined by the user.

        Returns
        ---------

        stopwords_tweets: str
            Normaliced version of the message.

        '''
        
        stopwords_es = set(stopwords.words('spanish'))
        stopwords_tweets = stopwords_es
        stopwords_tweets.update(self.other_stop_words)
        return stopwords_tweets

    def textToWords(self):

        '''
        Description: Create a list with all key words 
        of URL's texts of the given dataframe.
        The words are steemitized.
        
        '''
        stopwords_tweet = self.getStopWords()
        tokenizer = RegexpTokenizer('[a-zA-ZñÑ]+')
        stemmer = PorterStemmer()
        all_texts = []
        ls_texts = []
        
        for i in range(self.df.shape[0]):
            
            #Normalizing texts.

            text = self.df['text'].iloc[i]   
            text= text.lower()
            text = self.normalize(text)

            #Tokenize texts and normalize words.

            text= nltk.tokenize.word_tokenize(text)
            text = [w for w in text if not w in stopwords_tweet] 
            text = [re.sub(r'[^\w\s]', '', w) for w in text]
            
            ## Stemming Words
            
            text = [stemmer.stem(w) for w in text] 
            text = " ".join(text)
            ls_texts.append(text)
            
            
        for text in ls_texts:
            
            text = tokenizer.tokenize(text)
            all_texts.append(text)
            
        all_texts = list(itertools.chain(*all_texts))
        return all_texts

    def getFreqWords(self):

        '''

        Description: Calculate the frecuency for each word in a list of texts, and sorted it.

        '''
        all_texts = self.textToWords()
        all_texts = [text for text in all_texts if len(text)>3] #Exclude all words that have less than three words.
        
        #Calculate frequencies for words.

        frec_all_texts = nltk.FreqDist(all_texts)
        frec_all_texts

        #Create dataframe with frequencies and sorting by descending.
        
        df_all_texts = pd.DataFrame.from_dict(frec_all_texts, orient='index')
        df_all_texts['Word'] = df_all_texts.index
        df_all_texts.columns = ['Frec', 'Word']
        df_all_texts.head()

        df_all_texts.sort_values(by=['Frec'], ascending=False, inplace=True)
        df_all_texts.head()
        df_all_texts.reset_index(drop = True, inplace=True)
        df_all_texts.head()

        return df_all_texts

    def chartFreqWords(self, q_words=30):

        '''

        Description: Create a bar chart that shows frecuency words.

        '''

        df_all_texts = self.getFreqWords()
        plt.figure(figsize = (15,8))
        plot = sns.barplot(x  = df_all_texts.iloc[:q_words].Word, y = df_all_texts.iloc[:q_words].Frec)
        for item in plot.get_xticklabels():
            item.set_rotation(90)
        plt.show()
    
    def chartDistribution(self, y):

        '''
        Description: Create a chart that shows the distribution of instances for each range of age.

        '''
        df_age_range = pd.DataFrame(y, columns=['age_range'])
        distribution = pd.DataFrame(df_age_range['age_range'].value_counts())
        plt.bar(distribution.index, distribution['age_range'])
        plt.show()
        return distribution
        
    def vectorizeWords(self, max_features):

        '''
        Description: Create vectors for each word for Machine Learning purpose.

        '''

        # Usaremos solo las 1000 palabras con mas frecuencia en todo el corpus para generar los vectores
        max_features = max_features
        all_texts = list(self.df['text'].values)

        # Es decir que cada instancia tendrá 1000 features
        cou_vec = CountVectorizer(max_features=max_features)
        arr_texts = cou_vec.fit_transform(all_texts).toarray()
        all_words = cou_vec.get_feature_names()
        arr_texts = cou_vec.fit_transform(all_texts).toarray()
        return arr_texts, all_words
    
    def confusion(self,ytest,y_pred, names):

        '''
        Description: Create a confusiones matrix chart that visualizes the perfomance for each class in the classification task of the algorithm

        '''
        
        labels = ['13-17','18-24','25-34','35-49','50-64','65-xx']
        cm = confusion_matrix(ytest,y_pred, labels = labels)
        cm_report = classification_report(ytest,y_pred, labels = labels )

        f, ax = plt.subplots(figsize=(5,5))

        sns.heatmap(cm,annot=True,linewidth=.5,linecolor="r",fmt=".0f",ax=ax)
        plt.xlabel("y_pred")
        plt.ylabel("y_true")
        ax.set_xticklabels(names)
        ax.set_yticklabels(names)
        
        plt.show()
        print("Metrics Confusion Matrix:")
        print(cm_report)

    
    def balanceData(self, x, y):

        '''
        Description: Create sintetic instances to balance the dataset that contains multiples classes.

        '''

        ros = RandomOverSampler(random_state=42)

        x_ros, y_ros = ros.fit_resample(x, y)


        print('Original dataset shape', len(y))
        print('Resample dataset shape', len(y_ros))

        return x_ros, y_ros
    
    
    def modelTweets(self, model, max_features, balance_dataset='Y'):

        '''
        Description: Run a selected model based in a maximum number of features

        '''

        # Vectorize Dataset
        x, words = self.vectorizeWords(max_features)
        y = list(self.df['age_range'].values)
        names = list(self.df['age_range'].unique())

        print('Original distribution of the Data:')
        print(self.chartDistribution(y))
        

        #Split Dataset based on the willing of the user if wants to train with unbalanced dataset or not.
        
        if balance_dataset == 'Y':

            # split balanced dataset
            x_b, y_b = self.balanceData(x, y)
            
            print('Balanced distribution of the Data:')
            print(self.chartDistribution(y_b))

            xtrain, xtest, ytrain, ytest = train_test_split(x_b, y_b, test_size=0.2,random_state=42,stratify=y_b)
        
        else:
            
            # split ubalanced dataset
            xtrain, xtest, ytrain, ytest = train_test_split(x, y, test_size=0.2,random_state=42,stratify=y)

        #Train the selected model
        m = model
        m.fit(xtrain,ytrain)
        
        #Make predictions and evaluate performance of the model
        y_pred = m.predict(xtest)

        #Print the outputs.
        print('confusion matrix', self.confusion(ytest, y_pred, names ))
        

        return m